import 'dart:io';

import 'package:activity_record/model/data_base.dart';
import 'package:activity_record/pages/diy_home.dart';
//import 'package:firebase_core/firebase_core.dart';
//import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
//import 'package:cloud_firestore/cloud_firestore.dart';

void main() {
  // final FirebaseApp app = await FirebaseApp.configure(
  //   name: 'diyproject',
  //   options: FirebaseOptions(
  //     googleAppID: Platform.isIOS
  //         ? '1:911825121743:ios:a068e02dbb0454a0'
  //         : '1:911825121743:android:958a28327a1c612c',
  //     gcmSenderID: '911825121743',
  //     apiKey: 'AIzaSyDGsZxktr-2tpchWbINjIqx2Z_QEbVrd_c',
  //     projectID: 'diy-project-cc1f5',
  //   ),
  // );
  // final FirebaseStorage storage = FirebaseStorage(
  //     app: app, storageBucket: 'gs://diy-project-cc1f5.appspot.com/');
  // final Firestore fireStore = Firestore(app: app);
  // await fireStore.settings(timestampsInSnapshotsEnabled: true);
  runApp(
    new MyApp(),
  );
} //main函数是程序的主入口

class MyApp extends StatelessWidget {
  bool _isDarkTheme = false;

  DataBase _dataBase = new DataBase();

  final ThemeData dark = new ThemeData(
      primaryColor: Color(0xFFda7660),
      accentColor: Color(0xFFea6f5a),
      brightness: Brightness.dark);

  final ThemeData light = new ThemeData(
      primaryColor: Color(0xFFda7660),
//      primaryColor: Color(0xFFd95636),
      accentColor: Color(0xFFea6f5a),
      canvasColor: Colors.grey[100],
      brightness: Brightness.light);

  @override
  Widget build(BuildContext context) {
    //返回一个material规范的app
    return new MaterialApp(
      title: '你的DIY', //这个title是你打开任务管理器的时候显示的名字
      theme: _isDarkTheme ? dark : light,
      home: new DiyHome(
        db: _dataBase,
      ),
    );
  }
}
