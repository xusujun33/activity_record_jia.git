import 'package:activity_record/model/diy_image.dart';
import 'package:activity_record/model/diy_project.dart';

class Diy {
  DiyProject _diyProject;

  List<DiyImage> _imageDatas;

  Diy(this._diyProject, this._imageDatas);

  DiyProject get diyProject => _diyProject;
  List<DiyImage> get imageDatas => _imageDatas;
}
