

class DiyImage {
  int _diyId;

  int _imageId;

  //String _imageOriginalUrl;
  String _imageUrl;

  DiyImage(this._diyId, this._imageUrl);

  int get diyId => _diyId;
  int get imageId => _imageId;
  //String get imageOriginalUrl => _imageOriginalUrl;
  String get imageUrl => _imageUrl;

  Map<String, dynamic> toMap() {
    Map map = <String, dynamic>{
      'diyId': _diyId,
      //'imageOriginalUrl': _imageOriginalUrl,
      'imageUrl': _imageUrl,
    };
    if (_imageId != null) {
      map['imageId'] = _imageId;
    }
    return map;
  }

  DiyImage.fromMap(Map<String, dynamic> map,String path) {
    _imageId = map['imageId'];
    _diyId = map['diyId'];
    //_imageOriginalUrl = '$path${map['imageOriginalUrl']}';
    _imageUrl = '${path}${map['imageUrl']}';
  }
}
