import 'package:activity_record/my_flutter_app_icons.dart';
import 'package:activity_record/ui/photo_view.dart';
//import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';

import 'package:activity_record/model/diy.dart';
import 'package:activity_record/pages/diy_add_dialog.dart';
import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:transparent_image/transparent_image.dart';
import 'package:cached_network_image/cached_network_image.dart';

class DiyItemInfo extends StatefulWidget {
  DiyItemInfo(
      {Key key, @required this.db, @required this.diys, @required this.currentIndex})
      : super(key: key);

  final List<Diy> diys;
  final int currentIndex;
  final db;

  @override
  State<StatefulWidget> createState() => new DiyItemInfoState();
}

class DiyItemInfoState extends State<DiyItemInfo> {
  //进入编辑页面
  _editDiyProject() async {
    var result = await Navigator.of(context).push(
      Theme.of(context).platform == TargetPlatform.iOS
          ? new CupertinoPageRoute(
              fullscreenDialog: true,
              builder: (context) {
                return new DiyAddDialog(
                  db: widget.db,
                  diy: widget.diys[widget.currentIndex],
                );
              },
            )
          : new MaterialPageRoute(
              fullscreenDialog: true,
              builder: (context) {
                return new DiyAddDialog(
                  db: widget.db,
                  diy: widget.diys[widget.currentIndex],
                );
              },
            ),
    );
    setState(() {
      if (result != null) {
        widget.diys.removeAt(widget.currentIndex);
        widget.diys.insert(widget.currentIndex, result);
      }
    });
  }

  //点击图片进入查看视图
  _openImage(images, index) {
    Navigator.push(
        context,
        new PageRouteBuilder(
            transitionDuration: new Duration(milliseconds: 300),
            pageBuilder: (BuildContext context, Animation<double> animation,
                Animation<double> secondAnimation) {
              return new FadeTransition(
                opacity: animation,
                child: new PhotoViewGalleryShow(
                  images: images,
                  index: index,
                ),
              );
            }));
  }

  //图标和文本组合row
  Row iconAndText(IconData iconData, String text, {double size: 18.0}) {
    return new Row(
      children: <Widget>[
        new Icon(iconData, size: size),
        new SizedBox(
          width: 10.0,
        ),
        new Text(
          text,
        )
      ],
    );
  }

  //diy文字信息区域
  Widget buildDiyInfoColumn(BuildContext context) {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new ListTile(
          title: Row(
            children: <Widget>[
              new Text(
                widget.diys[widget.currentIndex].diyProject.name,
                style:
                    new TextStyle(fontWeight: FontWeight.bold, fontSize: 22.0),
              ),
            ],
          ),
          subtitle: widget.diys[widget.currentIndex].diyProject.notes != ''
              ? new Text(
                  '备注：${widget.diys[widget.currentIndex].diyProject.notes}',
                  style: new TextStyle(
                    fontWeight: FontWeight.w500,
                  ),
                )
              : new Text(
                  '备注：无',
                  style: new TextStyle(
                    fontWeight: FontWeight.w500,
                  ),
                ),
          trailing: widget.diys[widget.currentIndex].diyProject.isPaided == true
              ? new Container(
                  padding: const EdgeInsets.all(2.0),
                  child: new Text(
                    '已结款',
                    style: new TextStyle(
                        fontSize: 12.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                  decoration: new BoxDecoration(
                      color: Color(0xffbf5a40),
                      borderRadius: new BorderRadius.circular(4.0)),
                )
              : new Container(
                  padding: const EdgeInsets.all(2.0),
                  child: new Text(
                    '未结款',
                    style: new TextStyle(
                        fontSize: 12.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                  decoration: new BoxDecoration(
                      color: Color(0xffe3ac2d),
                      borderRadius: new BorderRadius.circular(4.0)),
                ),
        ),
        new Padding(
          padding: const EdgeInsets.only(left: 16.0),
          child: iconAndText(
            Xusj.my,
            widget.diys[widget.currentIndex].diyProject.contact,
          ),
        ),
        buildPadding(
          iconAndText(
            Xusj.time,
            widget.diys[widget.currentIndex].diyProject.date,
          ),
          iconAndText(
            Xusj.local1,
            widget.diys[widget.currentIndex].diyProject.place,
            size: 21.0,
          ),
        ),
        buildPadding(
          iconAndText(Xusj.money2,
              '${widget.diys[widget.currentIndex].diyProject.singlePrcie}元',
              size: 19.0),
          iconAndText(
              Xusj.cloudData, '${widget.diys[widget.currentIndex].diyProject.nums}份',
              size: 21.0),
        ),
        new SizedBox(
          height: 8.0,
        ),
        new Divider(
          height: 8.0,
          indent: 18.0,
        ),
        //活动金额信息
        new ListTile(
          title: new Text(
              '总价 : ${widget.diys[widget.currentIndex].diyProject.totalAmount}元'),
          subtitle: new Row(
            children: <Widget>[
              new Text(
                  '物料 : ${widget.diys[widget.currentIndex].diyProject.itemCost}元'),
              new SizedBox(
                width: 28.0,
              ),
              new Text(
                  '人员 : ${widget.diys[widget.currentIndex].diyProject.laborCost}元'),
            ],
          ),
          trailing: new Text(
            '利润 : ${widget.diys[widget.currentIndex].diyProject.profit}元',
            style: Theme.of(context).textTheme.title,
          ),
        ),
        new Divider(
          indent: 18.0,
        ),
        new Padding(
          padding: const EdgeInsets.only(left: 16.0, top: 8.0),
          child: new Text(
            '更多活动照片',
            style: new TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
      ],
    );
  }

  //时间地点、单价份数的padding包装
  Padding buildPadding(Widget iconAndTextFirst, Widget iconAndTextSecond) {
    return new Padding(
      padding: const EdgeInsets.only(left: 16.0, top: 14.0),
      child: new Row(
        children: <Widget>[
          new Expanded(child: iconAndTextFirst),
          new Expanded(child: iconAndTextSecond),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Material(
      child: new CustomScrollView(
        slivers: <Widget>[
          new SliverAppBar(
            expandedHeight: 260.0,
            pinned: true,
            flexibleSpace: new FlexibleSpaceBar(
              background: new FadeInImage(
                placeholder: new MemoryImage(kTransparentImage),
                image: new AssetImage(
                    widget.diys[widget.currentIndex].imageDatas.first.imageUrl),
                fit: BoxFit.cover,
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                padding: EdgeInsets.zero,
                child: new Text(
                  '编辑',
                  style: Theme.of(context).textTheme.subhead.copyWith(
                      color: Theme.of(context).scaffoldBackgroundColor),
                ),
                onPressed: _editDiyProject,
              )
            ],
          ),
          new SliverList(
            delegate:
                new SliverChildListDelegate([buildDiyInfoColumn(context)]),
          ),
          new SliverPadding(
            padding: const EdgeInsets.all(16.0),
            sliver: new SliverGrid.count(
              crossAxisCount: 2,
              crossAxisSpacing: 6.0,
              mainAxisSpacing: 6.0,
              childAspectRatio: 1.2,
              children: List.generate(
                  widget.diys[widget.currentIndex].imageDatas.length, (index) {
                return new GestureDetector(
                  onTap: () {
                    _openImage(widget.diys[widget.currentIndex].imageDatas, index);
                  },
                  child: new Hero(
                    tag: '$index',
                    child: new FadeInImage(
                      placeholder: new MemoryImage(kTransparentImage),
                      image: new AssetImage(
                          widget.diys[widget.currentIndex].imageDatas[index].imageUrl),
                      fit: BoxFit.cover,
                    ),
                  ),
                );
              }),
            ),
          )
        ],
      ),
    );
  }
}
