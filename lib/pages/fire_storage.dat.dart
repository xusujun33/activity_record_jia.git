//import 'package:flutter/material.dart';
//// import 'package:image_picker/image_picker.dart';
//import 'package:cached_network_image/cached_network_image.dart';
//import 'package:firebase_storage/firebase_storage.dart';
//import 'dart:math';
//import 'package:flutter/cupertino.dart';
//import 'package:multi_image_picker/multi_image_picker.dart';
//import 'dart:typed_data';
//import 'dart:typed_data';
//
//class FireStorageTest extends StatefulWidget {
//  final FirebaseStorage storage;
//  FireStorageTest({this.storage});
//  @override
//  State<StatefulWidget> createState() => FireStorageTestState();
//}
//
//class FireStorageTestState extends State<FireStorageTest> {
//  List<StorageUploadTask> _upLoadTaskList = [];
//  List<Uint8List> _listUt8 = [];
//  List<String> _urlList = [];
//
//  _upLoad() async {
//    var result =
//        await MultiImagePicker.pickImages(maxImages: 12, enableCamera: false);
//    _listUt8 = [];
//
//    if (result.length != 0) {
//      for (int i = 0; i < result.length; i++) {
//        await result[i].requestOriginal();
//        Uint8List imageData = result[i].imageData.buffer.asUint8List();
//        _listUt8.add(imageData);
//      }
//      if (_listUt8.length != 0) {
//        print('图片数据有：${_listUt8.length}');
//        for (int i = 0; i < _listUt8.length; i++) {
//          int radom = new Random().nextInt(100000);
//          print('开始创建第$i个上传任务');
//          StorageReference storageReference =
//              widget.storage.ref().child('image').child('image_${radom}.jpg');
//          StorageUploadTask storageUploadTask =
//              storageReference.putData(_listUt8[i]);
//          setState(() {
//            _upLoadTaskList.add(storageUploadTask);
//          });
//        }
//        _upLoadTaskList.forEach((task) async {
//          StorageTaskSnapshot snapshot = await task.onComplete;
//          String uri = await snapshot.ref.getDownloadURL();
//          setState(() {
//            _urlList.add(uri);
//          });
//
//          print('图片的url：$uri');
//        });
//      }
//    }
//  }
//
//  // _getUrl() {
//  //   _upLoadTaskList.forEach(
//  //     (value) async {
//  //       String url = await value.lastSnapshot.ref.getDownloadURL();
//  //       print('URL:$url');
//  //       setState(() {
//  //         _urlList.add(url);
//  //       });
//  //     },
//  //   );
//  // }
//
//  @override
//  Widget build(BuildContext context) {
////    final List<Widget> children = <Widget>[];
////    _upLoadTaskList.forEach((StorageUploadTask task) async {
////      final Widget tile = UpLoadTaskListTile(
////        task: task,
////        onDownLoad: () => _downLoad(task.lastSnapshot),
////      );
////      children.add(tile);
////    });
//    return new Scaffold(
//      appBar: new AppBar(
//        title: new Text('fireStorage'),
//      ),
//      body: Center(
//        child: new ListView(
//          children: [
//            new Column(
//              children: List.generate(
//                _upLoadTaskList.length,
//                (index) {
//                  return new UpLoadTaskListTile(
//                      task: _upLoadTaskList[index],
//                      url: _urlList.length == 0
//                          ? 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAsJCQcJCQcJCQkJCwkJCQkJCQsJCwsMCwsLDA0QDBEODQ4MEhkSJRodJR0ZHxwpKRYlNzU2GioyPi0pMBk7IRP/2wBDAQcICAsJCxULCxUsHRkdLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCz/wAARCAD8AQQDASIAAhEBAxEB/8QAHAABAQEBAQADAQAAAAAAAAAAAAMEBwYBBQgC/8QATxAAAAQCCAQCBQcHCQcFAAAAAAECAwQRBRNCYoGhweEGEiExM0EiUWFx8AcUMlWRlNMVFjU2dbG0FyQmRFJUY2RzI0NFgpLR0nJ0oqTx/8QAFwEBAQEBAAAAAAAAAAAAAAAAAAECA//EABsRAQEBAQEBAQEAAAAAAAAAAAABAhESMSEy/9oADAMBAAIRAxEAPwDrgAACL9jHQRFn7GOgiADYMY2AAi/Yx0FhF+xjoAiAAA2AAAIv2MdBEWfsY6CIANgxjYACL9jHQWEX7GOgCIAADYAAAi/Yx0ERZ+xjoIgA2DGNgAIv2MdBYRfsY6AIgAANgAAAAAAxgAALMW8NRYRYt4aiwAMY2DGACzFvDURFmLeGoCwAADGAAAsxbw1FhFi3hqLAAxjYMYALMW8NREWYt4agLAAAMYAACzFvDUWEWLeGosADGNgxgAsxbw1ERZi3hqAsAAAxgAAAAAC1Rfy3Cov5biwAI+De5sJSCvuZ7A/Yx0EQFq+5nsFRfy3ERsARqL+W4eDe5sJSFhF+xjoAV9zPYK+5nsIgAtUX8twqL+W4sACPg3ubCUgr7mewP2MdBEBavuZ7BUX8txEa5gJVF/LcPBvc2EpC0/YIv2MdACvuZ7BX3M9hEAFqi/luFRfy3FgAR8G9zYSkFfcz2B+xjoIgLV9zPYKi/luIjYAjUX8tw8G9zYSkLCL9jHQAr7mewV9zPYRABaov5bhUX8txYAEai/luAsAAAxgAs/Yx0ERZi3hqLAMY2AMYDYIv2MdBEWYt4agIgNgAADGACz9jHQRFmLeGosAxjBxTHRVG8P0zGQquWIaZQTS5TNCnXEM85e0uaZe4fdjyfGhf0Xp7/Shf4tkWftHKaBpelIKm6NiGop41REfCMxRLcWon2n3ktrS5zH17z9/Ud8fsYj850d+kqJ/aMB/EoMfoxi3hqN7EQGwBzABjABZ+xjoIizFvDUWAYxsAYwGwRfsY6CIsxbw1ARAbAAAGMAGwBjAAAWqL+W4VF/LcAYt4aiwj4N7mwlIK+5nsAsMYtX3M9gqL+W4CIsxbw1Cov5bh4N7mwlIBYfQ01xZQNArZZjXHVRDqScJiGbrHEtmZlzqmZJIunTrP2D7ivuZ7Dk3yh0NHopFym0IUuDikw7byk9fm7zbZMklc/I5TSfrn2mXPczo6TR9IQFKQrUbAvJdh3JkSi9FSVF3SpKpGRl8dDmeocJ4f4gpDh+Mr2ZrhnTQmMhjMyQ+nz69iWRT5T6eZdjkfbqMjqPpeDYjoF8nGHS6lIicaXIjNt1M+iin1LQ5ndTg3MW8NRYR8G9zYSkFfcz2GRYeT40/Vanv9KF/i2R6avuZ7DzvGzfLwtTp80/8AZwvl/mmjFz9HFaO/SVE/tKA/iED9GMW8NR+c6N60lRHtpKjy/wDsoIfozwb3NhKR7jexYBGvuZ7BX3M9hzEQ7/Hl6/8AuLVN8u0+2487xNxHAcOQxGo0v0i+k/mcL6zM+QnXZHMkT+0+hdjNKfo3xtO0FQvIVJRrUOt5JLbQolrcNM1FzcjSVKl06HKRj7KGi4WNYZiYR5p+HeTzNOsqJaFlMy6Gn1dj9pH6h+c46OjKQiYiNjX1OvvqNbriz+zoXQiLyIuhSKRS79f4BgqQoygSRHIW2uLi3o1llw5LaacQ2giURzkZmRqMvLnG9Z5B7QYxavuZ7BUX8txgRFmLeGoVF/LcPBvc2EpALAI19zPYK+5nsAiAtUX8twqL+W4CIC1Rfy3ABYAABF+xjoIiz9jHQRABsGMbAARfsY6Cwi/Yx0ARH8OtNPtOsvNocadQptxDhcyFoURkaVJPpL4939gHwcf4w4PdoJ1UbBJW5RLq5TmalQbizKTbhn1NJn9Ez93cy5/quHeIqQ4djK9ibkM6aUxkKozJD6PZ5Esinyn7y7HJXfHWWn23WXkpcadQptxtxJKQtCikaVEfkY4xxhwe7QTq42BStyiXVyn6Slwa1H0bdM+vKZ/ROfs6GZVnTOvU5UdZgqUo+mIOGjoF0nGHZkfbnbcIiM23UkfRRa+oyM7jhHD/ABBH8PxdewZuQ7hpTFwyjMm30eovIlF15T9/cjMldro2koGlYOHjoNznYeLpzERLQopkptxJGclFI5lMZ1nitg+n4468LU6RFMyah1SL+ymJaMzP2F3MfcDQ8wzENOsPIS4y6hbbja0kaFoWXKaTI/KQzPo/OVGEo6UoVKSNSlUnR5JJJGcz+co7dN/YP0W9Y/5tDHn6O4H4VouNRSENCuKiGlc0P84eceQwfrbSs+5eRnM/aPQP2MRrV6Ihgfl9h/HUB9DxJxJB8PQhLUSXo98lfMoUz7n1Kudl1JH7z6dJGaJJ0X4o4ogeHYYuYkvUi+hRwcLPvZrXZdkF5dZn2LsZo4dHx8ZSMTERsa8p2IeUa3XFy8+hJ6dJEXQiLsXbv1R8dGUjExMdGvKdffVWOuLlPp0JMi9EiIuhEXYuhe3pHBPBNV83pum2Cr08rtHwTierEvSJ99J2/NBeU59VEVX0kmBPg3gpTXzamqZZk9InoCCdT1YL6SXn0na80lZn1mZETfRBV4vonPp6unSXx/8AvYSHO20BsGMbBAEX7GOgsIv2MdAEQAAGwAAAAAAYwAAFmLeGosIsW8NRYAGMbBjABZi3hqIizFvDUBYAABjH8OtMvNuMvNocacQptxtxJKQpCiMjSZH0kP7AT4OVU18nlLIiluUG0iIg3OqWnH223YYzMyNClPGRGnpOZHP2eZ9A4RoOIoChmIKJdQ5EqdeiYiqMzbQ46ZHyNmrrIiIi7dR90yU+efs1mZfG95DVtsAYxsGMQBZi3hqIizFvDUBYcs4/4cp+NpRqkoGFiIyHdh2WFoYSTjjC2zUUjbI+aRzM5l5+X9rqY+Je39wS8o5lwfwUuGcapWm2ElEtqJcFBucqqgyOZPvSM085WCn0nPqZEbfQvd2nP4+MugfZ8ZgF0LMW8NRYRYt4aiwAMY2DGACzFvDURFmLeGoCwAADGAAAAAALVF/LcKi/luLAAj4N7mwlIK+5nsD9jHQRAWr7mewVF/LcRGwBCqLr6Xb2bj58H1q5sJSHjPlDpqlaKgqNZo5xxg4919D0Q0Zk4hLSUGTTai6kap+Uj9Hv6+XHT/Ep96apc/fHRfsvjWc9H6Errh/bsFfcP7dh+e/y7xL9dUr9+iv/ADD8u8S/XVK/for/AMxrwP0JUXv/AI7j4qb2W4/Pv5wcTfXdL/f4r8Qeh4O4k4j/AC/RcE9HRkZDx7q2XWop51+SatSzdbN0zMuUymcpefqmJccHYvBvc2EpBX3M9gfsY6CIwLV9zPYKi/luIjYAjUX8tw8G9zYSkLCL9jHQAr7mewV9zPYRABWpvZbj5qb2W4+t4ipc6EoePpJLaXXWSbQy2szJBuvLS0k1ykcinM+vWXcu45Kr5QONDMzKPaSUzkgoSFkRH2LqieYsz0ds8G9zYSkFfdL/AKthxI+PuNDlOkEHKf8AU4M/3tj4/P3jP+/t/coP8Ia8Udur7pf9Ww+Km9luOJfn9xn/AH9v7lB/hDTB/KNxTDPtOxrzUXCpNJvNGww0paJ+lyLaSkyV6j7esTxR2Wov5bh4N7mwlIWEX7GOgyFfcz2CvuZ7CIALVF/LcKi/luLAAjUX8twFgAAGMAFn7GOgiLMW8NRYBjGwBjAI+jaOpSGcg6Qh24iGcMlKbcI5cxdlEaTJRH7SMu5+secd4A4ITyyo5wvpT/nsdq8PRizFvDUPg8l+YfBf1e599jfxQ/MPgv6vd++xv4o9mAvaPK/yfcDfVrv36kPxhvorhXhmhXnImjqPbaiFoqzeWt550kdJklb61GU/OUhvAO0WfsY6CIsxbw1FhBjGwBjAbBF+xjoIizFvDUBEBsABljoGDpGEiYGLbrIaJQbbqJqSZlMjKRpkZGXSXuHh1fJZQRmrlpOlkpMzMiP5mZkXkU6ge2DAg7weFc+S6hUSlSlKdZ9yg/wh/H8mVDfWdJ/ZB/hDobFvDUWGvVHNv5MqG+s6T+yD/CG+D+TPhyGiWYh+JpCMS04lwmIhTCWlqT2raptKjL1lMiPzIx7oYw9UbBF+xjoIizFvDUZEQGwAABjABsAYwAAFqi/luFRfy3AGLeGosI+De5sJSCvuZ7ALDGLV9zPYKi/luAiLMW8NQqL+W4eDe5sJSAWARr7mewV9zPYBEPgu/wBnv+PdWqvZF/3HhOMuMUUUT1F0U6lVJGk0REQnqUElXQ0o6+J+72n0TZOj7yN4w4YoiLXAxcYo30mRPkw048iH9jqkEfUvMimfsHomX2Ihpl9hxDjLyEuNONmRoWhRTJRGQ/NjTUVGRDLDDbkRFxDvK00ianHHFdZzM8VHPp3Myl071w5Bv0RQlFUc+pLj8MwZOqQfoJcWpTpoT0I5FOSenkNazwfejGLV9zPYfFVey/f1GBIWYt4aj4qr3T3bj58G9zYSkAsPraQp2gqKWy1SEfDw7rxEpttajNZpnLm5UEZkn2mUunsGyvu57DivHsFSLFPRkXEJWcPHmhyEePqhTbbaEG36vROXSU+x9jmdzOjsLTrL7bbzLiHWnEpW240pK23EGU+ZCkGZGWI/scc4S4teoR1MHGqW5RLq5qLqtUItR+K2XflM/pEXqn0mfP2Rk2X2mX2XkuMvIS4043JSFoUUyUlRHKRhrPBVi3hqLCPg3ubCUgr7mewgsMYtX3M9gqL+W4CIsxbw1Cov5bh4N7mwlIBYBGvuZ7BX3M9gEQFqi/luFRfy3ARAWqL+W4ALAAAIv2MdBEWfsY6CIANgxjYACL9jHQWEX7GOgCISM+heuXn9nv8Aj3B4njLjD8lE7RdGKI6SWiUTEEc/maVl9Fs+1Yc8Jl3n6Nk6P74041TRhPUVRThKpFSTRFRCT5ig0n05Uf4n7vafRPJ2WYuMiGoeHbdiIuJd5Gmkek444ozMz69PWajPsUzOUppMtRcZEMw8O07ERcS7yNNIkpxxxZzPv0/9Rn2KZnKU09p4R4Qh+H2PnETyPUw+gifeT1Qwk5KOHYmX0S8zl19xESen8InwtwhD8Pw6YiJ5HqXfQZRDxdUMEclVDE7JWjl18+hEkvTfBT+PsFXrE/LmzEhzttUH0vGlNRdA0McVB8pRL8U1CMrWklkypaXHDc5VEZGZcpyn68D+6HkPlP8A1fgf2xD9/wD20T6gk7R9JwXxhT8ZTULRtIxPzpiNKIS2a0Nk4w420p6aVoSRyPllI55deoP2MdBwzgYv6WcPHP8A3kaR/cnx3N+xjoLqcoiMdI0bAUrCPwUa0TjLsj9S21l1JxtXkou5H7e0jkewBnvBwriPh2kOHow2H5uQzhrXCRRF6DyCPsoi6Esilzl7j6z6/acH8YO0C6mCjDW5RDy5qSU1LhHFH1cbIuvKZ/SSRe3vOs6/SdF0fS8G9AxzVYw6XuW2si9FxtR9SUXkf7yOR8N4j4cpHh2LqX5uQrhqVBxRJMkPpn0JRdicIvpF7j7H16516nKjutezENQ7zDiHGXUE4042olIWlREZKSZdJfHu+BxzhPi16hHEwcYa3aJdWZmRekqFWs+rjSS68pn9IsekzrOwMusvttvMuIdadQlxtxtRKStKimRkZfGnPWeK/sbBjGwQBF+xjoLCL9jHQBEAABsAAAAAAGMAABZi3hqLCLFvDUWABjGwYwAWYt4aiIsxbw1AWHI+KOCOJH6ZjoyjYdMZDx8QuImT7DSmlOekpDhPLLp6pGeEh1wfEvaLLyjxfCnCcPQLJvxHI/SryJPvp6pYR9I2GJ9iLzOXpefQuUvU/H2AAW2izFvDUWEWLeGosIA598pP6Bgv2vD+X+XiR0Ec++Un9AwP7Xh/4eJGs/R4Xgv9aaBP/EjP4N8d1Yt4DhXBf600D/qRn8G+O6sW/wDl1F39FgABgYxkpGjoClYR6CjWicYdLy9Fba5SJxtRdSUXr0OR6wDvBxOluDuIqPjHGIaCi4+HUucM/CMqWS0qM+WtSnolfTrORDrPCdGx1EUBRUDGnOJaS6t1BKJRNG68t0myMukkzl38h9wyU+fD3ecxaXWcxq6tHyMY2DGMgLMW8NREWYt4agLAAAMYAAAAAAtUX8twqL+W4sACPg3ubCUgr7mewP2MdBEBavuZ7BUX8txEbAEai/luHg3ubCUhYRfsY6AFfcz2CvuZ7CIALVF/LcKi/luLAAj4N7mwlIK+5nsD9jHQRAWr7mew8N8paOWgYLr/AMYhy9382iR7MZqdoSDp6jnaPiVKQk1IdZdblzsvInyrSR9PMyMvMjxFl5Rxrgkubirh8vW7GS7H/UnzHdPBvc2EpDyPDfAMHQMcdIvR6o2KQhxEKdQUO2xWFyLXy1izNRl0I+Yu59Ovo+ufsY6C6vaFfcz2CvuZ7CIDItUX8twqL+W4sACPg3ubCUgr7mewP2MdBEBavuZ7BUX8txEbAEai/luHg3ubCUhYRfsY6AFfcz2CvuZ7CIALVF/LcKi/luLAAjUX8twFgAAGMAFn7GOgiLMW8NRYBjGwBjAbBF+xjoIizFvDUBEBsAAAYwAWfsY6CIsxbw1FgGMbAGMBsEX7GOgiLMW8NQEQGwAABjABZ+xjoIizFvDUWAYxsAYwGwRfsY6CIsxbw1ARAbAAAGMAGwBjAAAWqL+W4VF/LcAYt4aiwj4N7mwlIK+5nsAsMYtX3M9gqL+W4CIsxbw1Cov5bh4N7mwlIBYBGvuZ7BX3M9gEQFqi/luFRfy3AGLeGosI+De5sJSCvuZ7ALDGLV9zPYKi/luAiLMW8NQqL+W4eDe5sJSAWARr7mewV9zPYBEBaov5bhUX8twBi3hqLCPg3ubCUgr7mewCwxi1fcz2Cov5bgIizFvDUKi/luHg3ubCUgFgEa+5nsFfcz2ARAWqL+W4VF/LcBEBaov5bgAsAAAi/Yx0ERZ+xjoIgA2DGNgAIv2MdBYRfsY6AIgAANgAACL9jHQRFn7GOgiADYMY2AAi/Yx0FhF+xjoAiAAA2AAAIv2MdBEWfsY6CIANgxjYACL9jHQWEX7GOgCIAADYAAAAAAP/2Q=='
//                          : _urlList[index],
//                      onCancel: () {
//                        setState(() {
//                          _upLoadTaskList.removeAt(index);
//                        });
//                      });
//                },
//              ),
//            ),
////            new Column(
////              children: List.generate(
////                _urlList.length,
////                (index) {
////                  return new CachedNetworkImage(
////                    height: 150.0,
////                    imageUrl: _urlList[index],
////                    placeholder: CupertinoActivityIndicator(),
////                    errorWidget: new Text('加载失败'),
////                  );
////                },
////              ),
////            )
//          ],
//        ),
//      ),
//      floatingActionButton: new FloatingActionButton(
//        onPressed: _upLoad,
//        child: Icon(Icons.image),
//      ),
//    );
//  }
//}
//
//class UpLoadTaskListTile extends StatelessWidget {
//  UpLoadTaskListTile(
//      {this.task,
////    this.onDismissed,
//      this.onCancel,
////    this.ut8,
//      this.url
////    this.getUrl,
//      });
//
//  final StorageUploadTask task;
////  final VoidCallback onDismissed;
//  final VoidCallback onCancel;
//  final String url;
////  final Uint8List ut8;
////  final VoidCallback getUrl;
//
//  get status {
//    String result;
//    if (task.isComplete) {
//      if (task.isSuccessful) {
//        result = '成功';
//      } else if (task.isCanceled) {
//        result = '取消';
//      } else
//        result = '失败:${task.lastSnapshot.error}';
//    } else if (task.isPaused) {
//      result = '暂停';
//    } else if (task.isInProgress) {
//      result = '上传中';
//    }
//    return result;
//  }
//
//  _bytePercent(StorageTaskSnapshot snapshot) {
//    return '${((snapshot.bytesTransferred / snapshot.totalByteCount) * 100).toInt()}%';
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return new StreamBuilder<StorageTaskEvent>(
//      stream: task.events,
//      builder: (BuildContext context,
//          AsyncSnapshot<StorageTaskEvent> asyncSnapShot) {
//        Widget subtitle;
//        if (asyncSnapShot.hasData) {
//          StorageTaskEvent event = asyncSnapShot.data;
//          StorageTaskSnapshot snapshot = event.snapshot;
//          subtitle = new Text('$status: ${_bytePercent(snapshot)}');
//        } else
//          subtitle = new Text('准备上传');
//        return new ListTile(
////          leading: new Image.memory(
////            ut8,
////            fit: BoxFit.cover,
////            width: 60.0,
////            height: 60.0,
////          ),
//          leading: new CachedNetworkImage(
//            imageUrl: url,
//            placeholder: CupertinoActivityIndicator(),
//            width: 60.0,
//            height: 60.0,
//          ),
//          title: new Text('上传任务：${task.hashCode}'),
//          subtitle: subtitle,
//          trailing: new Row(
//            mainAxisSize: MainAxisSize.min,
//            children: <Widget>[
//              new Offstage(
//                offstage: !task.isInProgress,
//                child: new IconButton(
//                  icon: Icon(Icons.pause),
//                  onPressed: () => task.pause(),
//                ),
//              ),
//              new Offstage(
//                offstage: !task.isPaused,
//                child: new IconButton(
//                  icon: Icon(Icons.file_upload),
//                  onPressed: () => task.resume(),
//                ),
//              ),
//              new Offstage(
//                offstage: task.isComplete,
//                child: new IconButton(
//                  icon: Icon(Icons.cancel),
//                  onPressed: onCancel,
//                ),
//              ),
//              new Offstage(
//                offstage: !(task.isComplete && task.isSuccessful),
//                child: new IconButton(
//                  icon: Icon(Icons.file_download),
//                  onPressed: () {},
//                ),
//              ),
//            ],
//          ),
//        );
//      },
//    );
//  }
//}
