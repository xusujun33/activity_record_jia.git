//import 'package:firebase_storage/firebase_storage.dart';
//import 'package:flutter/material.dart';
//
//import 'package:transparent_image/transparent_image.dart';
//
//class UpLoadTaskContainer extends StatelessWidget {
//  UpLoadTaskContainer({
//    this.task,
////    this.onDismissed,
//    this.onCancel,
//    this.imagePath,
////    this.index,
////    this.url
////    this.getUrl,
//  });
//
//  final StorageUploadTask task;
////  final VoidCallback onDismissed;
//  final VoidCallback onCancel;
////  final String url;
//  final String imagePath;
////  final int index;
////  final VoidCallback getUrl;
//
//  get status {
//    String result;
//    if (task.isComplete) {
//      if (task.isSuccessful) {
//        result = '成功';
//      } else if (task.isCanceled) {
//        result = '取消';
//      } else
//        result = '失败:${task.lastSnapshot.error}';
//    } else if (task.isPaused) {
//      result = '暂停';
//    } else if (task.isInProgress) {
//      result = '上传中';
//    }
//    return result;
//  }
//
//  _bytePercent(StorageTaskSnapshot snapshot) {
//    return '${((snapshot.bytesTransferred / snapshot.totalByteCount) * 100).toInt()}%';
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return new StreamBuilder<StorageTaskEvent>(
//      stream: task.events,
//      builder: (BuildContext context,
//          AsyncSnapshot<StorageTaskEvent> asyncSnapShot) {
//        Widget subtitle;
//        if (asyncSnapShot.hasData) {
//          StorageTaskEvent event = asyncSnapShot.data;
//          StorageTaskSnapshot snapshot = event.snapshot;
//          subtitle = new Text(
//            '$status:${_bytePercent(snapshot)}',
//            style: new TextStyle(color: Colors.white),
//          );
//        } else
//          subtitle = new Text(
//            '准备上传',
//            style: new TextStyle(color: Colors.white),
//          );
//        return new Stack(
//          alignment: Alignment.bottomCenter,
//          children: <Widget>[
//            new FadeInImage(
//              placeholder: new MemoryImage(kTransparentImage),
//              image: AssetImage(
//                imagePath,
//              ),
//              fit: BoxFit.cover,
//              width: (MediaQuery.of(context).size.width - 32.0 - 8.0) / 3,
//              height: (MediaQuery.of(context).size.width - 32.0 - 8.0) / 3,
//            ),
//            new Positioned(
//              right: 0.08,
//              top: 0.08,
//              child: new Offstage(
//                offstage: !(task.isComplete && task.isSuccessful),
//                child: new GestureDetector(
//                  onTap: onCancel,
//                  child: new Container(
//                    decoration: new BoxDecoration(
//                      color: Colors.black45,
//                      shape: BoxShape.circle,
//                    ),
//                    child: new Icon(
//                      Icons.close,
//                      color: Colors.white,
//                      size: 20.0,
//                    ),
//                  ),
//                ),
//              ),
//            ),
//            new Positioned(
//                child: new Container(
//              alignment: Alignment.center,
//              height: 20.0,
//              width: (MediaQuery.of(context).size.width - 32.0 - 8.0) / 3,
//              color: Colors.black45,
//              child: subtitle,
//            )),
//          ],
//        );
//      },
//    );
//  }
//}
